from django.forms import Select


class FrSelectWhitQuery(Select):
    def __init__(self, url='', attrs=None, choices=()):
        self.url = url
        super().__init__(attrs, choices)

    def get_context(self, name, value, attrs):
        widget_class = attrs.get('class', '') + ' ' + self.attrs.get('class', '')
        widget_class += ' fr_select2'
        attrs.update({'class': widget_class, 'data-ajax--url': self.url})
        self.choices.queryset = self.choices.queryset.none()
        context = super().get_context(name, value, attrs)
        return context

    class Media:
        js = ('iqsd-utilities/js/frSelectWhitQuery.js',)
