$('select.fr_select2').select2({
    theme: 'bootstrap',
    language: 'es',
    ajax: {
        data: function (params) {
            return {
                search: params.term,
                limit: 10
            }
        },
        processResults: function (data) {
            return {
                results: data.results.map((val => {return {id: val.id, text: val.nombre}}))
            };
        },
        delay: 250,
        minimumInputLength: 2,
    }
})