from django.urls import reverse


class RedirectSuccessMixin:
    redirects_views = {
        'xxx': 'xxx-id'
    }

    def get_success_url(self):
        redirect_view = self.kwargs.get('redirect')
        params = self.kwargs.get('redirect_params')
        try:
            return reverse(self.redirects_views[redirect_view], args=[params])
        except KeyError:
            return super().get_success_url()
