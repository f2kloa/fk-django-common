import io

from django.http import FileResponse
from reportlab.lib.enums import TA_JUSTIFY
from reportlab.lib.pagesizes import LETTER
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import cm
from reportlab.platypus import Frame, PageTemplate, BaseDocTemplate


class PdfMixin:
    estilo = getSampleStyleSheet()
    pdf_name = 'common'
    response = None
    side_margin = cm
    header_space = 5 * cm

    def __init__(self, **kwargs):
        self.pdf_name = kwargs.get('pdf_name', 'common')
        self.estilo['Normal'].alignment = TA_JUSTIFY
        buffer = io.BytesIO()
        self.create_pdf(buffer)
        buffer.seek(0)
        self.response = FileResponse(buffer, as_attachment=True, filename=f'{self.pdf_name}.pdf')

    def get_encabezado(self, canvas, doc):
        canvas.saveState()
        canvas.setFont('Helvetica-Bold', 9)
        y = LETTER[1] - 50
        canvas.drawString(5.2 * cm, y, "Encabezado")
        canvas.restoreState()

    def get_pie(self, canvas, doc):
        canvas.saveState()
        canvas.setFont('Times-Roman', 9)
        canvas.drawString(cm, 0.75 * cm, "Página %d" % doc.page)
        canvas.restoreState()

        # pdf generation logic here
        # open an existing pdf or generate one using i.e. reportlab

    def get_story(self):
        """override story"""
        return []

    def create_pdf(self, buffer):
        story = self.get_story()
        templates = self.get_page_templates()
        doc = BaseDocTemplate(buffer, pageTemplates=templates,
                              pagesize=LETTER)
        doc.build(story)
        return doc

    def get_frames(self):
        return {'frameN': Frame(self.side_margin, cm, LETTER[0] - self.side_margin * 2,
                                LETTER[1] - self.header_space,
                                id='normal')}

    def get_page_templates(self):
        frames = self.get_frames()
        return [
            PageTemplate(id='UnaColumna', frames=frames['frameN'], onPage=self.get_encabezado,
                         onPageEnd=self.get_pie)
        ]
